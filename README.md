# Computed Field Dispatcher

The module implements:
- Computed Field PHP Hook Formatter
- The event dispatcher for Computed Field module using Hook Event Dispatcher module

## Install

Add above setting into `repositories` of `composer.json`, then run `composer require drupal/computed_field_dispatcher`

```json
{
  "type": "vcs",
  "url": "https://git.drupalcode.org/sandbox/agrozyme-3157163.git",
  "package": {
    "name": "drupal/computed_field_dispatcher",
    "description": "Computed Field Dispatcher",
  }
}
```

## Using
We assume you create a custom module: `example` and add the `example` computed field in a content type

You need do 3 things:
- create Event Subscriber class
- create hook functions in `example.module`
- add the Event Subscriber class in `example.service.yml`

### Event Subscriber

```php
<?php
declare(strict_types=1);

namespace Drupal\example\EventSubscriber\ComputedField;

use Drupal;
use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldFormatEvent as FormatEvent;
use Drupal\Core\TypedData\ComplexDataInterface;
use RtLopez\Decimal;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class Example
 */
class Example implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents(): array {
    return [
      'hook_event_dispatcher.computed_field.node.example.compute' => 'compute',
      'hook_event_dispatcher.computed_field.node.example.format' => 'format',
    ];
  }

  /**
   * @param ComputeEvent $computeEvent
   *
   * @return bool
   */
  function compute(ComputeEvent $computeEvent): bool {
    $computeEvent->setValue('example');
    return true;
  }

  /**
   * @param FormatEvent $formatEvent
   *
   * @return bool
   */
  function format(FormatEvent $formatEvent): bool {
    $formatEvent->setValue('example');
    return true;
  }

}
```

### Hook Fnctions
It still need to create `computed_field_{$field_name}_compute` / `computed_field_{$field_name}_format` functions

```php
<?php
declare(strict_types=1);

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Field\FieldItemInterface;
use function hook_computed_field_dispatcher_computed_field_compute as compute;
use function hook_computed_field_dispatcher_computed_field_format as format;

/**
 * @param EntityTypeManager $entity_type_manager
 * @param EntityInterface   $entity
 * @param array             $fields
 * @param int               $delta
 *
 * @return mixed
 */
function computed_field_example_compute(
  EntityTypeManager $entity_type_manager,
  EntityInterface $entity,
  array $fields,
  int $delta
) {
  $field_name = 'example';
  return compute($entity_type_manager, $entity, $fields, $delta, $field_name);
}

/**
 * @param mixed              $value_raw
 * @param string             $value_escaped
 * @param FieldItemInterface $field_item
 * @param int                $delta
 * @param string             $langcode
 *
 * @return string
 */
function computed_field_example_format(
  $value_raw,
  string $value_escaped,
  FieldItemInterface $field_item,
  int $delta,
  string $langcode
) {
  return format($value_raw, $value_escaped, $field_item, $delta, $langcode);
}
```

### .services.yml

```yaml
services:

  Drupal\example\EventSubscriber\ComputedField\Example:
    tags:
      - { name: event_subscriber }
```
