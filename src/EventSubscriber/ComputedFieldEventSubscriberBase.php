<?php
declare(strict_types=1);

namespace Drupal\computed_field_dispatcher\EventSubscriber;

use Drupal;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldComputeEvent as ComputeEvent;
use Drupal\computed_field_dispatcher\Event\ComputedField\ComputedFieldFormatEvent as FormatEvent;
use Drupal\Core\TypedData\ComplexDataInterface;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class BaseComputedFieldEventSubscriber
 */
abstract class ComputedFieldEventSubscriberBase implements EventSubscriberInterface {
  const SCALE = 10;

  /**
   * @param FormatEvent $formatEvent
   *
   * @return bool
   */
  public function format(FormatEvent $formatEvent): bool {
    $entity = $formatEvent->getFieldItem()->getEntity();
    $computeEvent = new ComputeEvent($entity, $formatEvent->getDelta(), $formatEvent->getFieldName());
    $test = $this->compute($computeEvent);
    $value = $test ? strval($computeEvent->getValue()) : '';
    $formatEvent->setValue($value);
    return $test;
  }

  /**
   * @param ComputeEvent $computeEvent
   *
   * @return bool
   */
  public function compute(ComputeEvent $computeEvent): bool {
    $data = $computeEvent->getEntity()->getTypedData();
    return $this->setProperties($data);
  }

  /**
   * @param ComplexDataInterface $item
   *
   * @return bool
   */
  protected function setProperties(ComplexDataInterface $item): bool {
    try {
      $values = $this->getValues($item);
      $test = isset($values);

      foreach ($values as $name => $value) {
        $test = $test && $this->setProperty($name, $value);
      }
    } catch (Exception $exception) {
      $test = false;
    }

    return $test;
  }

  /**
   * @param ComplexDataInterface $item
   *
   * @return array
   */
  abstract protected function getValues(ComplexDataInterface $item): array;

  /**
   * @param string $name
   * @param mixed $value
   *
   * @return bool
   */
  protected function setProperty(string $name, $value): bool {
    if (property_exists($this, $name) && isset($value)) {
      $this->{$name} = $value;
      return true;
    }

    return false;
  }

  /**
   * @param string $entity_type
   * @param mixed $id
   *
   * @return ComplexDataInterface
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function loadEntity(string $entity_type, $id): ComplexDataInterface {
    return Drupal::entityTypeManager()->getStorage($entity_type)->load($id)->getTypedData();
  }

}
