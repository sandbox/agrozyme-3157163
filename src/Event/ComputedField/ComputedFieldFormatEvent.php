<?php
declare(strict_types=1);

namespace Drupal\computed_field_dispatcher\Event\ComputedField;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\hook_event_dispatcher\Event\EventInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ComputedFieldFormatEvent
 */
class ComputedFieldFormatEvent extends Event implements EventInterface {
  protected $valueRaw;
  protected $fieldItem;
  protected $delta;
  protected $langcode;

  protected $value = '';

  /**
   * @param mixed $valueRaw
   * @param FieldItemInterface $fieldItem
   * @param int $delta
   * @param string $langcode
   */
  public function __construct($valueRaw, FieldItemInterface $fieldItem, int $delta, string $langcode) {
    $this->valueRaw = $valueRaw;
    $this->fieldItem = $fieldItem;
    $this->delta = $delta;
    $this->langcode = $langcode;
  }

  /**
   * {@inheritdoc}
   */
  public function getDispatcherType(): string {
    return 'hook_event_dispatcher.computed_field.' . $this->getEntityType() . '.' . $this->getFieldName() . '.format';
  }

  /**
   * @return string
   */
  public function getEntityType(): string {
    return $this->getFieldItem()->getEntity()->getEntityTypeId();
  }

  /**
   * @return FieldItemInterface
   */
  public function getFieldItem(): FieldItemInterface {
    return $this->fieldItem;
  }

  /**
   * @return string
   */
  public function getFieldName(): string {
    return $this->getFieldItem()->getFieldDefinition()->getName();
  }

  /**
   * @return mixed
   */
  public function getValueRaw() {
    return $this->valueRaw;
  }

  /**
   * @return int
   */
  public function getDelta(): int {
    return $this->delta;
  }

  /**
   * @return string
   */
  public function getLangcode(): string {
    return $this->langcode;
  }

  /**
   * @return string
   */
  public function getValue(): string {
    return $this->value;
  }

  /**
   * @param string $value
   */
  public function setValue(string $value) {
    $this->value = $value;
  }

}
