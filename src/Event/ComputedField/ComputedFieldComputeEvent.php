<?php
declare(strict_types=1);

namespace Drupal\computed_field_dispatcher\Event\ComputedField;

use Drupal\Core\Entity\EntityInterface;
use Drupal\hook_event_dispatcher\Event\EventInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ComputedFieldComputeEvent
 */
class ComputedFieldComputeEvent extends Event implements EventInterface {
  protected $entity;
  protected $delta;
  protected $fieldName;

  protected $value = '';

  /**
   * @param EntityInterface $entity
   * @param int $delta
   * @param string $fieldName
   */
  public function __construct(EntityInterface $entity, int $delta, string $fieldName) {
    $this->entity = $entity;
    $this->delta = $delta;
    $this->fieldName = $fieldName;
  }

  /**
   * {@inheritdoc}
   */
  public function getDispatcherType(): string {
    return 'hook_event_dispatcher.computed_field.' . $this->getEntityType() . '.' . $this->getFieldName() . '.compute';
  }

  /**
   * @return string
   */
  public function getEntityType(): string {
    return $this->getEntity()->getEntityTypeId();
  }

  /**
   * @return EntityInterface
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * @return string
   */
  public function getFieldName(): string {
    return $this->fieldName;
  }

  /**
   * @return int
   */
  public function getDelta(): int {
    return $this->delta;
  }

  /**
   * @return mixed
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * @param mixed $value
   */
  public function setValue($value) {
    $this->value = $value;
  }

}
