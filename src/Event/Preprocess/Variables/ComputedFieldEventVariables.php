<?php
declare(strict_types=1);

namespace Drupal\computed_field_dispatcher\Event\Preprocess\Variables;

use Drupal\preprocess_event_dispatcher\Variables\AbstractEventVariables;

/**
 * Class ComputedFieldEventVariables
 */
class ComputedFieldEventVariables extends AbstractEventVariables {

}
