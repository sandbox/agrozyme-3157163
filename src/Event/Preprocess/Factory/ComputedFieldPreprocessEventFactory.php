<?php
declare(strict_types=1);

namespace Drupal\computed_field_dispatcher\Event\Preprocess\Factory;

use Drupal\computed_field_dispatcher\Event\Preprocess\ComputedFieldPreprocessEvent;
use Drupal\computed_field_dispatcher\Event\Preprocess\Variables\ComputedFieldEventVariables;
use Drupal\preprocess_event_dispatcher\Event\AbstractPreprocessEvent;
use Drupal\preprocess_event_dispatcher\Factory\PreprocessEventFactoryInterface;

/**
 * Class ComputedFieldPreprocessEventFactory
 */
class ComputedFieldPreprocessEventFactory implements PreprocessEventFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function createEvent(array &$variables): AbstractPreprocessEvent {
    return new ComputedFieldPreprocessEvent(new ComputedFieldEventVariables($variables));
  }

  /**
   * {@inheritdoc}
   */
  public function getEventHook(): string {
    return ComputedFieldPreprocessEvent::getHook();
  }
}
