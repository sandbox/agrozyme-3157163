<?php
declare(strict_types=1);

namespace Drupal\computed_field_dispatcher\Event\Preprocess;

use Drupal\preprocess_event_dispatcher\Event\AbstractPreprocessEvent;

/**
 * Class ComputedFieldPreprocessEvent
 */
class ComputedFieldPreprocessEvent extends AbstractPreprocessEvent {

  /**
   * {@inheritdoc}
   */
  public static function getHook(): string {
    return 'computed_field';
  }
}
